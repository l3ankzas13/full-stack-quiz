const puppeteer = require('puppeteer');
const cheerio = require('cheerio');
const fetch = require('node-fetch');

const BASE_URL = 'https://codequiz.azurewebsites.net';

async function runWithPuppeteer({ targetFundName }) {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto(BASE_URL);
  await page.click('input[type=button]');
  await page.goto(BASE_URL);

  const fundsArray = await page.evaluate(
    () => Array.from(document.querySelectorAll('tr'), (trElement, i) => {
      if (i === 0) {
        return Array.from(trElement.querySelectorAll('th'), (thElement) => thElement.innerHTML)
      }
      return Array.from(trElement.querySelectorAll('td'), (tdElement) => tdElement.innerHTML)
    })
  );

  await browser.close();

  let header = []
  const funds = fundsArray.reduce((preventFundsArray, item, index) => {
    const result = item.reduce((preventFund, fund, fundIndex) => {
      if (index === 0) {
        header.push(fund)
        return preventFund
      }
      return { ...preventFund, [header[fundIndex].trim()]: fund.trim() }
    }, {})
    if (index === 0) return preventFundsArray
    return preventFundsArray.concat(result)
  }, [])

  const fund = funds.find((item) =>
    item['Fund Name'].toUpperCase() === targetFundName.toUpperCase().trim()
  )
  console.log('[puppeteer] - Nav: ', fund ? fund['Nav'] : '')
}

async function runWithNodeFetchCookie({ targetFundName }) {
  const element = await (await fetch(BASE_URL)).text();
  const $ = cheerio.load(element)
  const cookieScript = $('script:not([src])')[0].children[0].data
  const cookieValue = cookieScript.match(/document.cookie = '(.*?)'/)[1]
  const foundsPageElement = await (await fetch(BASE_URL, {
    headers: { accept: '*/*', cookie: cookieValue }
  })).text();
  const $foundsEl = cheerio.load(foundsPageElement)
  let header = []
  let funds = []
  $foundsEl('body > table > tbody > tr').each((index, element) => {
    if (index === 0) {
      const ths = $(element).find("th");
      $(ths).each((i, element) => {
        header.push($(element).text());
      });
      return true;
    }
    const tds = $foundsEl(element).find('td');
    let body = {}
    $(tds).each((i, element) => {
      body[header[i].trim()] = $(element).text().trim();
    });
    funds.push(body)
  });

  const fund = funds.find((item) =>
    item['Fund Name'].toUpperCase() === targetFundName.toUpperCase().trim()
  )

  console.log('[fetch with cookie] - Nav: ', fund ? fund['Nav'] : '')
}

(async () => {
  const targetFundName = process.argv[process.argv.length - 1]
  await runWithPuppeteer({ targetFundName });
  await runWithNodeFetchCookie({ targetFundName });
})();