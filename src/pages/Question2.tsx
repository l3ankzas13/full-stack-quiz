import { useEffect, useState, ChangeEvent } from 'react';

const API_URL = 'https://api.publicapis.org/categories';

interface Props {
  goTo: (text: string) => (event: React.MouseEvent<HTMLElement>) => void
}

function Question2({ goTo }: Props) {
  const [originalData, setOriginalData] = useState<Array<string>>([])
  const [dataFiltered, setFilterData] = useState<Array<string>>([])
  const [searchValue, setSearchValue] = useState<string | undefined>('')
  const [loading, setLoading] = useState<boolean>(false)

  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      let result = undefined;
      try {
        const response = await (await fetch(API_URL)).json()
        result = response
      } catch (error) {
        console.log('error', error)
      } finally {
        setOriginalData(result)
        setLoading(false)
      }
    }
    fetchData()
  }, [])

  useEffect(() => {
    if (searchValue) {
      const result = originalData.filter((value) => value.toUpperCase().includes(searchValue.toUpperCase())) 
      setFilterData(result)
    } else {
      setFilterData(originalData)
    }
  }, [searchValue, originalData])

  const handleChange = (event: ChangeEvent<HTMLInputElement>): void => {
    event.preventDefault()
    setSearchValue(event?.target?.value)
  }

  return (
    <>
      <p 
        className="menu"
        onClick={goTo('question1')}
        style={{ textAlign: 'center' }}
      >
        Go To Question 1
      </p>
      <div className="q2-container">
        <div className="input-group">
          <label className="label">Search:</label>
          <input name="search" onChange={handleChange} />
        </div>
        <h3>Result: {loading && 'Loading...'}</h3>
        <div className="content">
          {dataFiltered.map(word => (
            <p key={word}>{word}</p>
          ))}
        </div>
      </div>
    </>
  )
}

export default Question2;