import { useState, useEffect, ChangeEvent } from 'react';
import * as math from '../utils/math';

type Form = {
  calNumber: number | string;
  calType: string;
}

interface Props {
  goTo: (text: string) => (event: React.MouseEvent<HTMLElement>) => void
}

function Question1({ goTo }: Props) {
  const [form, setForm] = useState<Form>({ calNumber: '', calType: 'isPrime'  })
  const [calResult, setCalResult] = useState<string>('')
  
  const handleOnChange = (event: ChangeEvent<HTMLInputElement | HTMLSelectElement>): void => {
    const { value, name } = event.target
    if (name === 'calNumber') {
      setForm(form => ({ ...form, [name]: value ? Math.abs(Number(value)) : '' }))
    } else {
      setForm(form => ({ ...form, [name]: value }))
    }
  }

  useEffect(() => {
    if (form.calNumber && form.calType) {
      const value = Math.abs(Number(form.calNumber))
      if (form.calType === 'isFibonacci') {
        return setCalResult(math.isFibonacci(value).toString())
      }
      if (form.calType === 'isPrime') {
        return setCalResult(math.isPrime(value).toString())
      }
    }
    return setCalResult('')
  }, [form])

  return (
    <>
      <p 
        className="menu"
        style={{ textAlign: 'center' }}
        onClick={goTo('question2')}
      >
        Go To Question 2
      </p>
      <div className="container">
        <div className="box1">
          <input
            value={form.calNumber}
            id="calNumber"
            name="calNumber"
            type="number"
            onChange={handleOnChange}
          />
        </div>
        <div className="box2">
          <select
            defaultValue="isPrime"
            id="calType"
            name="calType"
            onChange={handleOnChange}
          >
            <option value="isPrime">isPrime</option>
            <option value="isFibonacci">isFibonacci</option>
          </select>
        </div>
        <div className="box3">{calResult}</div>
      </div>
    </>
  )
}

export default Question1;