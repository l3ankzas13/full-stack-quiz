import React, { useState } from 'react';
import './App.css';

import Question1 from './pages/Question1';
import Question2 from './pages/Question2';

function App() {
  const [currentPage, setCurrentPage] = useState<String>('')

  const handlePageClick = (name: String) => () => {
    setCurrentPage(name)
  }

  return (
    <>
      {!currentPage && (
        <div className="section">
          <div>
          <div className="question">
          <h1>Section 2</h1>
            <p className="menu" onClick={handlePageClick('question1')}>Go To Question 1</p>
            <p className="menu" onClick={handlePageClick('question2')}>Go To Question 2</p>
          </div>
          </div>
        </div>
      )}
      {currentPage === 'question1' && <Question1 goTo={handlePageClick} />}
      {currentPage === 'question2' && <Question2 goTo={handlePageClick} />}
    </>
  );
}

export default App;
