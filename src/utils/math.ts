const isSquare = (numberToCheck: number): boolean => {
  return numberToCheck > 0 && Math.sqrt(numberToCheck) % 1 === 0;
};

const isFibonacci = (numberToCheck: number): boolean => {
  return isSquare(5 * numberToCheck * numberToCheck + 4)
    || isSquare(5 * numberToCheck * numberToCheck - 4);
}

const isPrime = (num: number): boolean => {
  for (let i = 2; i < num; i++)
    if (num % i === 0) return false;
  return num > 1;
}

export {
  isSquare,
  isFibonacci,
  isPrime
}