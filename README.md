# Refinitiv Quize with React and NodeJs
<br/>

### front-end Quiz with React
---
**step1: install dependencies**
- ``yarn install``

**step2: start section2 question 1 & question2**
- ``yarn start``

**step3: go to web application**
- ``[http:localhost:3000](http://localhost:3000)``
  
**step4: click menu question1 or question2 on page for see answer**

<br/>

### Backend Quiz with NodeJS
---
> I have 2 solutions to solve this problem.
> 1. Run with Puppeteer
> 2. Run with Fetch and Cheerio

**step1: install dependencies (if you install dependencies in front-end Quiz with React, You don't need to install it again.)**
- ``yarn install``

**step2: Run**
- ``node server/index.js BEQSSF``




